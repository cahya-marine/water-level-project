package entity

import (
	"context"
	"time"

	"github.com/uptrace/bun"
	"gitlab.com/cahya-marine/water-level-project/pkg/response"
)

type Actual struct {
	bun.BaseModel               `bun:"table:actual"`
	ID                          int64      `bun:",pk,autoincrement,nullzero" json:"id"`
	WaterLevelTelukSiwak        float64    `bun:",nullzero" json:"water_level_teluk_siwak" csv:"water_level_teluk_siwak"`
	WaterLevelBintangNinggi     float64    `bun:",nullzero" json:"water_level_bintang_ninggi" csv:"water_level_bintang_ninggi"`
	Datetime                    CustomDate `bun:",nullzero" json:"datetime" csv:"datetime"`
	Date                        int        `bun:",nullzero" json:"date" csv:"date"`
	Month                       string     `bun:",nullzero" json:"month" csv:"month"`
	Hours                       string     `bun:",nullzero" json:"hours" csv:"hours"`
	Year                        int        `bun:",nullzero" json:"year" csv:"year"`
	D2WeatherPurukCahu          string     `bun:"d_2_weather_puruk_cahu,nullzero" json:"d_2_weather_puruk_cahu" csv:"d_2_weather_puruk_cahu"`
	D1WeatherPurukCahu          string     `bun:"d_1_weather_puruk_cahu,nullzero" json:"d_1_weather_puruk_cahu" csv:"d_1_weather_puruk_cahu"`
	D2WeatherMuaraTaweh         string     `bun:"d_2_weather_muara_taweh,nullzero" json:"d_2_weather_muara_taweh" csv:"d_2_weather_muara_taweh"`
	D1WeatherMuaraTaweh         string     `bun:"d_1_weather_muara_taweh,nullzero" json:"d_1_weather_muara_taweh" csv:"d_1_weather_muara_taweh"`
	ActualWeatherPurukCahu      string     `bun:",nullzero" json:"actual_weather_puruk_cahu" csv:"actual_weather_puruk_cahu"`
	ActualWeatherJamut          string     `bun:",nullzero" json:"actual_weather_jamut" csv:"actual_weather_jamut"`
	ActualWeatherTahup          string     `bun:",nullzero" json:"actual_weather_tuhup" csv:"actual_weather_tahup"`
	ActualWeatherMuaraTaweh     string     `bun:",nullzero" json:"actual_weather_muara_teweh" csv:"actual_weather_muara_taweh"`
	ActualWeatherBintangNinggi  string     `bun:",nullzero" json:"actual_weather_bintang_ninggi" csv:"actual_weather_bintang_ninggi"`
	WaterLevelDiffPurukCahu     float64    `bun:",nullzero" json:"water_level_diff_puruk_cahu" csv:"water_level_diff_puruk_cahu"`
	WaterLevelDiffJamut         float64    `bun:",nullzero" json:"water_level_diff_jamut" csv:"water_level_diff_jamut"`
	WaterLevelDiffTuhup         float64    `bun:",nullzero" json:"water_level_diff_tuhup" csv:"water_level_diff_tahup"`
	WaterLevelDiffMuaraTaweh    float64    `bun:",nullzero" json:"water_level_diff_muara_taweh" csv:"water_level_diff_muara_taweh"`
	WaterLevelDiffBintangNinggi float64    `bun:",nullzero" json:"water_level_diff_bintang_ninggi" csv:"water_level_diff_bintang_ninggi"`
	WaterLevelDiffTerusan       float64    `bun:",nullzero" bun:",nullzero" json:"water_level_diff_terusan" csv:"water_level_diff_terusan"`
	WaterLevelPurukCahu         float64    `bun:",nullzero" json:"water_level_puruk_cahu" csv:"water_level_puruk_cahu"`
	WaterLevelJamut             float64    `bun:",nullzero" json:"water_level_jamut" csv:"water_level_jamut"`
	WaterLevelTuhup             float64    `bun:",nullzero" json:"water_level_tuhup" csv:"water_level_tuhup"`
	WaterLevelMuaraTaweh        float64    `bun:",nullzero" json:"water_level_muara_taweh" csv:"water_level_muara_taweh"`
	WaterLevelTerusan           float64    `bun:",nullzero" json:"water_level_terusan" csv:"water_level_terusan"`
	CreatedAt                   time.Time  `json:"created_at" csv:"created_at"`
	UpdatedAt                   time.Time  `json:"updated_at" csv:"updated_at"`
}

type ActualResponse struct {
	bun.BaseModel               `bun:"table:actual"`
	ID                          int64     `bun:",pk,autoincrement,nullzero" json:"id"`
	WaterLevelTelukSiwak        float64   `json:"water_level_teluk_siwak" csv:"water_level_teluk_siwak"`
	WaterLevelBintangNinggi     float64   `json:"water_level_bintang_ninggi" csv:"water_level_bintang_ninggi"`
	Datetime                    time.Time `json:"datetime" csv:"datetime"`
	Date                        int       `json:"date" csv:"date"`
	Month                       string    `json:"month" csv:"month"`
	Hours                       string    `json:"hours" csv:"hours"`
	Year                        int       `json:"year" csv:"year"`
	D2WeatherPurukCahu          string    `bun:"d_2_weather_puruk_cahu" json:"d_2_weather_puruk_cahu" csv:"d_2_weather_puruk_cahu"`
	D1WeatherPurukCahu          string    `bun:"d_1_weather_puruk_cahu" json:"d_1_weather_puruk_cahu" csv:"d_1_weather_puruk_cahu"`
	D2WeatherMuaraTaweh         string    `bun:"d_2_weather_muara_taweh" json:"d_2_weather_muara_teweh" csv:"d_2_weather_muara_taweh"`
	D1WeatherMuaraTaweh         string    `bun:"d_1_weather_muara_taweh" json:"d_1_weather_muara_teweh" csv:"d_1_weather_muara_taweh"`
	ActualWeatherPurukCahu      string    `json:"actual_weather_puruk_cahu" csv:"actual_weather_puruk_cahu"`
	ActualWeatherJamut          string    `json:"actual_weather_jamut" csv:"actual_weather_jamut"`
	ActualWeatherTahup          string    `json:"actual_weather_tuhup" csv:"actual_weather_tahup"`
	ActualWeatherMuaraTaweh     string    `json:"actual_weather_muara_teweh" csv:"actual_weather_muara_taweh"`
	ActualWeatherBintangNinggi  string    `json:"actual_weather_bintang_ninggi" csv:"actual_weather_bintang_ninggi"`
	WaterLevelDiffPurukCahu     float64   `json:"water_level_diff_puruk_cahu" csv:"water_level_diff_puruk_cahu"`
	WaterLevelDiffJamut         float64   `json:"water_level_diff_jamut" csv:"water_level_diff_jamut"`
	WaterLevelDiffTuhup         float64   `json:"water_level_diff_tuhup" csv:"water_level_diff_tuhup"`
	WaterLevelDiffMuaraTaweh    float64   `json:"water_level_diff_muara_teweh" csv:"water_level_diff_muara_taweh"`
	WaterLevelDiffBintangNinggi float64   `json:"water_level_diff_bintang_ninggi" csv:"water_level_diff_bintang_ninggi"`
	WaterLevelDiffTerusan       float64   `json:"water_level_diff_terusan" csv:"water_level_diff_terusan"`
	WaterLevelPurukCahu         float64   `json:"water_level_puruk_cahu" csv:"water_level_puruk_cahu"`
	WaterLevelJamut             float64   `json:"water_level_jamut" csv:"water_level_jamut"`
	WaterLevelTuhup             float64   `json:"water_level_tuhup" csv:"water_level_tuhup"`
	WaterLevelMuaraTaweh        float64   `json:"water_level_muara_teweh" csv:"water_level_muara_taweh"`
	WaterLevelTerusan           float64   `json:"water_level_terusan" csv:"water_level_terusan"`
	CreatedAt                   time.Time `json:"created_at" csv:"created_at"`
	UpdatedAt                   time.Time `json:"updated_at" csv:"updated_at"`
}

func (m *Actual) BeforeAppendModel(ctx context.Context, query bun.Query) error {
	switch query.(type) {
	case *bun.InsertQuery:
		m.CreatedAt = time.Now()
		m.UpdatedAt = time.Now()
	case *bun.UpdateQuery:
		m.UpdatedAt = time.Now()
	}
	return nil
}

type ActualDataRequest struct {
	ID      int64     `json:"id"`
	Date    time.Time `json:"date"`
	RawData []struct {
		Location   string  `json:"location"`
		Weather    string  `json:"weather"`
		WaterLevel float64 `json:"water_level"`
	} `json:"raw_data"`
}
type GetActualParams struct {
	response.PagingResponse
	response.PaginationQuery
	Date string `form:"date"`
}

type GetActualData struct {
	Date time.Time `form:"date"`
}

type Data struct {
	Location       string  `json:"location"`
	Weather        string  `json:"weather"`
	WaterLevel     float64 `json:"water_level"`
	WaterLevelDiff float64 `json:"water_level_diff"`
}

func ToActualResponse(res ActualResponse) []Data {

	rawData := make([]Data, 0, 6)
	purukCahu := Data{
		Location:       "Puruk Cahu",
		Weather:        res.ActualWeatherPurukCahu,
		WaterLevel:     res.WaterLevelPurukCahu,
		WaterLevelDiff: res.WaterLevelDiffPurukCahu,
	}
	jamut := Data{
		Location:       "Jamut",
		Weather:        res.ActualWeatherJamut,
		WaterLevel:     res.WaterLevelJamut,
		WaterLevelDiff: res.WaterLevelDiffJamut,
	}
	tuhup := Data{
		Location:       "Tuhup",
		Weather:        res.ActualWeatherTahup,
		WaterLevel:     res.WaterLevelTuhup,
		WaterLevelDiff: res.WaterLevelDiffTuhup,
	}
	muaraTeweh := Data{
		Location:       "Muara Teweh",
		Weather:        res.ActualWeatherMuaraTaweh,
		WaterLevel:     res.WaterLevelMuaraTaweh,
		WaterLevelDiff: res.WaterLevelDiffMuaraTaweh,
	}
	bintangNinggi := Data{
		Location:       "Bintang Ninggi",
		Weather:        res.ActualWeatherBintangNinggi,
		WaterLevel:     res.WaterLevelBintangNinggi,
		WaterLevelDiff: res.WaterLevelDiffBintangNinggi,
	}
	siwak := Data{
		Location:   "Teluk Siwak",
		WaterLevel: res.WaterLevelTelukSiwak,
	}

	terusan := Data{
		Location:       "Terusan",
		WaterLevel:     res.WaterLevelTerusan,
		WaterLevelDiff: res.WaterLevelDiffTerusan,
	}

	rawData = append(rawData, purukCahu, jamut, tuhup, muaraTeweh, bintangNinggi, siwak, terusan)
	return rawData
}
