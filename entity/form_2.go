package entity

import (
	"context"
	"github.com/uptrace/bun"
	"time"
)

type Form2 struct {
	bun.BaseModel              `bun:"table:form_2"`
	ID                         int64     `bun:",pk,autoincrement,nullzero" json:"id"`
	TelukSiwak                 float64   `json:"teluk_siwak" csv:"water_level_teluk_siwak"`
	BintangNinggi              float64   `json:"bintang_ninggi" csv:"water_level_bintang_ninggi"`
	Date                       int       `json:"date" csv:"date"`
	Month                      string    `json:"month" csv:"month"`
	D2WeatherPurukCahu         string    `bun:"d_2_weather_puruk_cahu" json:"d_2_weather_puruk_cahu" csv:"d_2_weather_puruk_cahu"`
	D1WeatherPurukCahu         string    `bun:"d_1_weather_puruk_cahu" json:"d_1_weather_puruk_cahu" csv:"d_1_weather_puruk_cahu"`
	D2WeatherMuaraTaweh        string    `bun:"d_2_weather_muara_taweh" json:"d_2_weather_muara_taweh" csv:"d_2_weather_muara_taweh"`
	D1WeatherMuaraTaweh        string    `bun:"d_1_weather_muara_taweh" json:"d_1_weather_muara_taweh" csv:"d_1_weather_muara_taweh"`
	ActualWeatherPurukCahu     string    `json:"actual_weather_puruk_cahu" csv:"actual_weather_puruk_cahu"`
	ActualWeatherJamut         string    `json:"actual_weather_jamut" csv:"actual_weather_jamut"`
	ActualWeatherTahup         string    `json:"actual_weather_tahup" csv:"actual_weather_tahup"`
	ActualWeatherMuaraTaweh    string    `json:"actual_weather_muara_taweh" csv:"actual_weather_muara_taweh"`
	ActualWeatherBintangNinggi string    `json:"actual_weather_bintang_ninggi" csv:"actual_weather_bintang_ninggi"`
	CreatedAt                  time.Time `json:"created_at" csv:"created_at"`
	UpdatedAt                  time.Time `json:"updated_at" csv:"updated_at"`
}

func (m *Form2) BeforeAppendModel(ctx context.Context, query bun.Query) error {
	switch query.(type) {
	case *bun.InsertQuery:
		m.CreatedAt = time.Now()
		m.UpdatedAt = time.Now()
	case *bun.UpdateQuery:
		m.UpdatedAt = time.Now()
	}
	return nil
}
