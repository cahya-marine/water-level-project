package entity

import (
	"context"
	"github.com/uptrace/bun"
	"time"
)

type Form1 struct {
	bun.BaseModel               `bun:"table:form_1"`
	ID                          int64      `bun:",pk,autoincrement,nullzero" json:"id"`
	WaterLevelTelukSiwak        float64    `json:"water_level_teluk_siwak" csv:"water_level_teluk_siwak"`
	WaterLevelBintangNinggi     float64    `json:"water_level_bintang_ninggi" csv:"water_level_bintang_ninggi"`
	Datetime                    CustomDate `json:"datetime" csv:"datetime"`
	Date                        int        `json:"date" csv:"date"`
	Month                       string     `json:"month" csv:"month"`
	Year                        int        `json:"year" csv:"year"`
	Hours                       string     `json:"hours" csv:"hours"`
	D2WeatherPurukCahu          string     `bun:"d_2_weather_puruk_cahu" json:"d_2_weather_puruk_cahu" csv:"d_2_weather_puruk_cahu"`
	D1WeatherPurukCahu          string     `bun:"d_1_weather_puruk_cahu" json:"d_1_weather_puruk_cahu" csv:"d_1_weather_puruk_cahu"`
	D2WeatherMuaraTaweh         string     `bun:"d_2_weather_muara_taweh" json:"d_2_weather_muara_taweh" csv:"d_2_weather_muara_taweh"`
	D1WeatherMuaraTaweh         string     `bun:"d_1_weather_muara_taweh" json:"d_1_weather_muara_taweh" csv:"d_1_weather_muara_taweh"`
	ActualWeatherPurukCahu      string     `json:"actual_weather_puruk_cahu" csv:"actual_weather_puruk_cahu"`
	ActualWeatherJamut          string     `json:"actual_weather_jamut" csv:"actual_weather_jamut"`
	ActualWeatherTahup          string     `json:"actual_weather_tahup" csv:"actual_weather_tahup"`
	ActualWeatherMuaraTaweh     string     `json:"actual_weather_muara_taweh" csv:"actual_weather_muara_taweh"`
	ActualWeatherBintangNinggi  string     `json:"actual_weather_bintang_ninggi" csv:"actual_weather_bintang_ninggi"`
	WaterLevelDiffPurukCahu     float64    `json:"water_level_diff_puruk_cahu" csv:"water_level_diff_puruk_cahu"`
	WaterLevelDiffJamut         float64    `json:"water_level_diff_jamut" csv:"water_level_diff_jamut"`
	WaterLevelDiffTuhup         float64    `json:"water_level_diff_tuhup" csv:"water_level_diff_tuhup"`
	WaterLevelDiffMuaraTaweh    float64    `json:"water_level_diff_muara_taweh" csv:"water_level_diff_muara_taweh"`
	WaterLevelDiffBintangNinggi float64    `json:"water_level_diff_bintang_ninggi" csv:"water_level_diff_bintang_ninggi"`
	WaterLevelDiffTerusan       float64    `json:"water_level_diff_terusan" csv:"water_level_diff_terusan"`
	CreatedAt                   time.Time  `json:"created_at" csv:"created_at"`
	UpdatedAt                   time.Time  `json:"updated_at" csv:"updated_at"`
}

func (m *Form1) BeforeAppendModel(ctx context.Context, query bun.Query) error {
	switch query.(type) {
	case *bun.InsertQuery:
		m.CreatedAt = time.Now()
		m.UpdatedAt = time.Now()
	case *bun.UpdateQuery:
		m.UpdatedAt = time.Now()
	}
	return nil
}
