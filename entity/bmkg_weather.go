package entity

import (
	"context"
	"github.com/uptrace/bun"
	"time"
)

type BMKGWeatherPrediction struct {
	ID          int64     `bun:",pk,autoincrement,nullzero" json:"id"`
	Location    string    `json:"location"`
	Datetime    time.Time `json:"datetime"`
	Temperature float64   `json:"temperature"`
	Weather     string    `json:"weather"`
	Humidity    float64   `json:"humidity"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

func (m *BMKGWeatherPrediction) BeforeAppendModel(ctx context.Context, query bun.Query) error {
	switch query.(type) {
	case *bun.InsertQuery:
		m.CreatedAt = time.Now()
		m.UpdatedAt = time.Now()
	case *bun.UpdateQuery:
		m.UpdatedAt = time.Now()
	}
	return nil
}
