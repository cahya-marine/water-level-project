package entity

import "time"

// CustomDate is a wrapper for time.Time to implement custom unmarshalling
type CustomDate struct {
	time.Time
	string
}

// UnmarshalCSV implements the CSV unmarshalling for CustomDate
func (cd *CustomDate) UnmarshalCSV(csv string) (err error) {
	if csv == "" {
		cd.Time = time.Time{}
		cd.string = ""
		return nil
	}
	cd.Time, err = time.Parse("2 January 2006 15:04", csv)
	return err
}
