package response

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/cahya-marine/water-level-project/pkg/errors"
)

type Response struct {
	StatusCode int         `json:"-"`
	ErrorCode  string      `json:"-"`
	Code       int         `json:"code"`
	Message    string      `json:"message"`
	Result     interface{} `json:"data"`
}

func HandleError(err error, c *gin.Context) *Response {
	ginErr := c.Error(err)

	switch typeErr := ginErr.Err.(type) {
	case errors.InternalError:
		return generateFailure(http.StatusInternalServerError, typeErr.GetCode(), typeErr.Cause.Error())
	case errors.BadRequestError:
		return generateFailure(http.StatusBadRequest, typeErr.GetCode(), typeErr.Cause.Error())
	case errors.UnauthorizedError:
		return generateFailure(http.StatusUnauthorized, typeErr.GetCode(), typeErr.Cause.Error())
	case errors.NotFoundError:
		return generateFailure(http.StatusNotFound, typeErr.GetCode(), typeErr.Cause.Error())
	default:
		unexpectedErr := errors.NewInternalError(ginErr.Err, "unexpected error")
		return generateFailure(http.StatusInternalServerError, unexpectedErr.GetCode(), unexpectedErr.Cause.Error())
	}
}

func generateFailure(statusCode int, errorCode string, errMsg string) *Response {
	return &Response{
		StatusCode: statusCode,
		ErrorCode:  errorCode,
		Message:    errMsg,
		Result:     nil,
	}
}

func HandleSuccess(data interface{}, code int) *Response {
	return &Response{
		Code:    code,
		Message: "OK",
		Result:  data,
	}
}

func HandleHTTPError(ctx *gin.Context, err error) {
	var res *Response
	res = HandleError(err, ctx)
	ctx.AbortWithStatusJSON(res.StatusCode, res)
}

func HandleHTTPSuccess(ctx *gin.Context, code int, data interface{}) {
	res := HandleSuccess(data, code)
	ctx.JSON(code, res)
}
