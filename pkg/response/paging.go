package response

import (
	"math"
)

type PaginationQuery struct {
	Page       int    `json:"-" form:"page,default=1" binding:"gte=0"`
	Limit      int    `json:"-" form:"limit,default=10" binding:"oneof=10 20 50 100 1000"`
	OrderField string `json:"-" form:"order_field,default=updated_at"`
	OrderBy    string `json:"-" form:"order_by,default=DESC" binding:"oneof=ASC DESC"`
}

type PagingResponse struct {
	TotalRecord  int         `json:"total_record"`
	NumberRecord int         `json:"number_record"`
	TotalPage    int         `json:"total_page"`
	CurrentPage  int         `json:"current_page"`
	PerPage      int         `json:"per_page"`
	HasNext      bool        `json:"has_next"`
	HasPrev      bool        `json:"has_prev"`
	Records      interface{} `json:"records"`
}

func CalculateOffset(limit int, page int) int {
	pageToCalculate := page - 1
	return pageToCalculate * limit
}
func NewPagingResponse(limit int, page int, totalRecord int, totalPageRecord int, data interface{}) (res PagingResponse) {
	offset := CalculateOffset(limit, page)

	res.TotalRecord = totalRecord
	res.NumberRecord = totalPageRecord
	res.TotalPage = int(math.Ceil(float64(totalRecord) / float64(limit)))
	res.CurrentPage = (offset / limit) + 1
	res.PerPage = limit
	res.HasNext = res.TotalPage >= 1 && res.TotalPage > res.CurrentPage
	res.HasPrev = res.TotalPage >= 1 && res.TotalPage+1 >= res.CurrentPage && res.CurrentPage > 1
	res.Records = data
	return
}
