package gin_internal

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	internal_errs "gitlab.com/cahya-marine/water-level-project/pkg/errors"
)

type RequestWithAdditionalValidation interface {
	Validate(ctx *gin.Context) []map[string]interface{}
}

func BindJSONBody[T any](ctx *gin.Context) (T, error) {
	var target T

	carriedError := ctx.ShouldBindBodyWith(&target, binding.JSON)
	if carriedError != nil {
		return target, internal_errs.NewBadRequestError(carriedError)
	}

	return target, nil
}

func BindParams[T any](ctx *gin.Context) (T, error) {
	var target T

	carriedError := ctx.ShouldBindQuery(&target)
	if carriedError != nil {
		return target, internal_errs.NewBadRequestError(carriedError)
	}

	return target, nil
}
