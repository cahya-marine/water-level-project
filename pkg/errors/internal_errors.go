package errors

import (
	"fmt"
	"strings"
)

// InternalError holds errors which should not be exposed to clients
type InternalError struct {
	BaseError
	Cause error
}

func (ierr InternalError) Error() string {
	return fmt.Sprintf("%s caused by: %s", ierr.Message, ierr.Cause.Error())
}

// NewInternalError wraps original error with optional detailed messages and gives InternalError
func NewInternalError(cause error, msg ...string) InternalError {
	ierr := InternalError{
		BaseError: BaseError{
			Code:    INTERNAL_ERROR,
			Message: "Internal Error",
		},
		Cause: cause,
	}
	if len(msg) > 0 {
		ierr.Message = strings.TrimSpace(strings.Join(msg, ", "))
	}
	return ierr
}
