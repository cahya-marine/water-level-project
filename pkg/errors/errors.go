package errors

import (
	"fmt"
)

// BaseError holds code and description of an error
type BaseError struct {
	Code    string `json:"code"`
	Message string `json:"message" binding:"required"`
}

func (err BaseError) Error() string {
	return fmt.Sprintf("[%s]%s\n", err.Code, err.Message)
}

func (err BaseError) GetCode() string {
	return err.Code
}
