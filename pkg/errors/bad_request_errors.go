package errors

import (
	"fmt"
	"strings"
)

// BadRequestError contains generic base error and the actual error
type BadRequestError struct {
	BaseError
	Cause error
}

func (bdr BadRequestError) Error() string {
	return fmt.Sprintf("%s caused by: %s", bdr.Message, bdr.Cause.Error())
}

func NewBadRequestError(cause error, msg ...string) BadRequestError {
	newError := BadRequestError{
		BaseError: BaseError{
			Code:    BAD_REQUEST_ERROR,
			Message: "Bad Request",
		},
		Cause: cause,
	}
	if len(msg) > 0 {
		newError.Message = strings.TrimSpace(strings.Join(msg, ", "))
	}
	return newError
}
