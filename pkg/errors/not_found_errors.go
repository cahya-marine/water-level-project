package errors

import (
	"fmt"
	"strings"
)

// BadRequestError contains generic base error and the actual error
type NotFoundError struct {
	BaseError
	Cause error
}

func (bdr NotFoundError) Error() string {
	return fmt.Sprintf("%s caused by: %s", bdr.Message, bdr.Cause.Error())
}

func NewNotFoundError(cause error, msg ...string) error {
	newError := NotFoundError{
		BaseError: BaseError{
			Code:    NOT_FOUND_ERROR,
			Message: "Not Found Error",
		},
		Cause: cause,
	}
	if len(msg) > 0 {
		newError.Message = strings.TrimSpace(strings.Join(msg, ", "))
	}
	return newError
}
