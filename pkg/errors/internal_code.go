package errors

import (
	"fmt"

	"github.com/pkg/errors"
)

// nolint:revive
const (
	BAD_REQUEST_ERROR = "BAD_REQUEST_ERROR"
	INTERNAL_ERROR    = "INTERNAL_ERROR"
	NOT_FOUND_ERROR   = "NOT_FOUND_ERROR"
	UNAUTHORIZED      = "UNAUTHORIZED"
)
const MsgFileFormatError string = "Invalid file format, expected : %s, actual : %s"

var (
	InvalidAuthorization     error = errors.New("Authorization is invalid")
	InvalidHardware          error = errors.New("You cannot access this hardwares")
	RoleInvalidAuthorization       = errors.New("Your role is not eligible")
	InvalidToken                   = errors.New("Token is invalid")
	InactiveAccount                = errors.New("Account is not activated")
	EmailOrPasswordInvalid         = errors.New("Email or password is wrong")
	ExpiredToken                   = errors.New("Token has been expired")
	DataNotFound                   = errors.New("Data not found")
	DocumentExceededSize           = errors.New("Document size is too large")
	ErrorNotLogin                  = errors.New("Please login first")
	InvalidDocumentFormat          = func(actual string, expected []string) error {
		return errors.New(fmt.Sprintf(MsgFileFormatError, actual, expected))
	}
)
