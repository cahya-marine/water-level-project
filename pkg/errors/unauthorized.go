package errors

import (
	"fmt"
	"strings"
)

// UnauthorizedError contains generic base error and the actual error
type UnauthorizedError struct {
	BaseError
	Cause error
}

func (uer UnauthorizedError) Error() string {
	return fmt.Sprintf("%s caused by: %s", uer.Message, uer.Cause.Error())
}

func NewUnauthorizedError(cause error, msg ...string) UnauthorizedError {
	newError := UnauthorizedError{
		BaseError: BaseError{
			Code:    UNAUTHORIZED,
			Message: "Unauthorized",
		},
		Cause: cause,
	}
	if len(msg) > 0 {
		newError.Message = strings.TrimSpace(strings.Join(msg, ", "))
	}
	return newError
}
