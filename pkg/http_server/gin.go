package http_server

import (
	"context"
	"errors"
	routers "gitlab.com/cahya-marine/water-level-project/http_server/router"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/cahya-marine/water-level-project/config"
)

func EnableCors() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Headers", "Authorization, Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Content-Length, Access-Control-Request-Method, Access-Control-Request-Headers")
		c.Header("Access-Control-Allow-Methods", "POST,HEAD,PATCH,OPTIONS,GET,PUT,DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

type HttpServer struct {
	handler    *gin.Engine
	httpServer *http.Server
}

func InitAndStart(c config.Application) *HttpServer {
	router := gin.New()
	routeManager := routers.NewRouteManager(router)
	routeManager.Register()
	router.GET("/", func(c *gin.Context) { c.JSON(200, gin.H{"asa": "asas"}) })

	srv := &http.Server{
		Addr:    ":" + c.AppPort,
		Handler: router,
	}

	go func() {
		log.Printf("Listening on port : %s", c.AppPort)
		if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("failed to start server %s\n", err)
		}
	}()

	return &HttpServer{
		handler:    router,
		httpServer: srv,
	}
}

func (s *HttpServer) Shutdown(ctx context.Context) error {
	return s.httpServer.Shutdown(ctx)
}
