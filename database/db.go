package db

import (
	"fmt"
	"sync"

	"github.com/XSAM/otelsql"
	_ "github.com/lib/pq"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
	"github.com/uptrace/bun/extra/bundebug"
	"gitlab.com/cahya-marine/water-level-project/config"
)

var dbInstance *bun.DB
var once sync.Once

func InitLogger(db *bun.DB, debug bool, level int) {
	db.AddQueryHook(
		bundebug.NewQueryHook(
			bundebug.WithEnabled(debug),
			bundebug.WithVerbose(level == 2),
		),
	)
}

func InitDB(c *config.Database) *bun.DB {
	once.Do(func() {
		dsn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=%s",
			c.Username, c.Password, c.Host, c.Port, c.Name, c.SSLMode)

		sqlDb := otelsql.OpenDB(
			pgdriver.NewConnector(pgdriver.WithDSN(dsn)),
		)

		dbInstance = bun.NewDB(sqlDb, pgdialect.New())
		InitLogger(dbInstance, c.Debug, c.DebugLevel)
	})

	return dbInstance
}

// GetConn return database connection instance
func GetConn() *bun.DB {
	return dbInstance
}
