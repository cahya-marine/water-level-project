package seeder

import (
	"log"
	"reflect"
	"strings"

	"gitlab.com/cahya-marine/water-level-project/repository"

	"gitlab.com/cahya-marine/water-level-project/config"
	db "gitlab.com/cahya-marine/water-level-project/database"
)

type Repo struct {
	Form1  *repository.Form1Repository
	Form2  *repository.Form2Repository
	Actual *repository.ActualRepository
}

type Seed struct {
	RepoSeeder Repo
}

func NewSeed() *Seed {
	c := config.InitializeConfig()
	db.InitDB(&c.Database)
	s := Seed{
		RepoSeeder: Repo{
			Form1:  repository.NewForm1Repository(),
			Form2:  repository.NewForm2Repository(),
			Actual: repository.NewActualRepository(),
		},
	}

	return &s
}

func Exec(seedNames ...string) {
	s := NewSeed()
	for _, name := range seedNames {
		if strings.HasSuffix(name, "Seeder") {
			seedThis(s, name)
		}
	}

}
func seedThis(s *Seed, method string) {
	meth := reflect.ValueOf(s).MethodByName(method)
	if !meth.IsValid() {
		log.Fatal("The given method name does not exist")
	}
	log.Println("Seeding", method, "...")
	meth.Call(nil)
	log.Println("Seed", method, "done")
}
