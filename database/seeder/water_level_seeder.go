package seeder

import (
	"context"
	"database/sql"
	"github.com/gocarina/gocsv"
	db "gitlab.com/cahya-marine/water-level-project/database"
	"gitlab.com/cahya-marine/water-level-project/entity"
	"log"
	"os"
	"path/filepath"
)

func (s *Seed) WaterLevelAnalysisSeeder() {
	ctx := context.Background()

	files := []string{
		"database/seeder/file/hack datasheet - form 1.csv",
		"database/seeder/file/hack datasheet - form 2.csv",
		"database/seeder/file/hack datasheet - actual.csv",
	}

	var err error
	tx, err := db.GetConn().BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		filePath, err := filepath.Abs(file)
		if err != nil {
			log.Fatal(err)
		}

		f, err := os.Open(filePath)
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		switch true {
		case file == "database/seeder/file/hack datasheet - form 1.csv":
			var form1Records []*entity.Form1
			if err := gocsv.UnmarshalFile(f, &form1Records); err != nil {
				log.Fatal(err)
			}

			if err := s.RepoSeeder.Form1.Create(ctx, &tx, form1Records); err != nil {
				if err := tx.Rollback(); err != nil {
					log.Fatal(err)
				}
				log.Fatal(err)
			}

		case file == "database/seeder/file/hack datasheet - form 2.csv":
			var form2Records []*entity.Form2
			if err := gocsv.UnmarshalFile(f, &form2Records); err != nil {
				log.Fatal(err)
			}

			if err := s.RepoSeeder.Form2.Create(ctx, &tx, form2Records); err != nil {
				if err := tx.Rollback(); err != nil {
					log.Fatal(err)
				}
				log.Fatal(err)
			}

		case file == "database/seeder/file/hack datasheet - actual.csv":
			var actual []*entity.Actual
			if err := gocsv.UnmarshalFile(f, &actual); err != nil {
				log.Fatal(err)
			}

			if err := s.RepoSeeder.Actual.Create(ctx, &tx, actual); err != nil {
				if err := tx.Rollback(); err != nil {
					log.Fatal(err)
				}
				log.Fatal(err)
			}

		}
	}

	if err := tx.Commit(); err != nil {
		log.Fatal(err)
	}
}
