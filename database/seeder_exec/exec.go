package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/cahya-marine/water-level-project/database/seeder"
)

func main() {
	flag.Parse()
	args := flag.Args()

	if len(args) >= 1 {
		switch args[0] {
		case "seed":
			seeder.Exec(args[1:]...)
		default:
			pusage()
		}
	} else {
		pusage()
	}
}

func pusage() {
	fmt.Println(`Usage: seed [seedNames...]`)
	os.Exit(2)
}
