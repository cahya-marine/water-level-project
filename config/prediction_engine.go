package config

type PredictionEngine struct {
	PredictionEngineURL string `env:"PREDICTION_ENGINE_URL"`
}
