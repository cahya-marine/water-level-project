package config

type Database struct {
	Username   string `env:"DB_USERNAME"`
	Password   string `env:"DB_PASSWORD"`
	Host       string `env:"DB_HOST"`
	Port       string `env:"DB_PORT"`
	Name       string `env:"DB_NAME"`
	Debug      bool   `env:"DB_DEBUG"`
	DebugLevel int    `env:"DB_DEBUG_LEVEL"`
	SSLMode    string `env:"DB_SSL_MODE"`
}
