package config

import (
	"log"

	"github.com/ilyakaznacheev/cleanenv"
)

var GlobalConfig Config

const path string = "./config/.env"

type Config struct {
	Application Application
	Database    Database
}

func InitializeConfig() Config {
	var cfg Config

	err := cleanenv.ReadConfig(path, &cfg)

	if err != nil {
		log.Fatal(err.Error())
	}
	GlobalConfig = cfg
	return cfg
}
