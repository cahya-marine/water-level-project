package config

type Application struct {
	AppPort      string `env:"APP_PORT"`
	AppEnv       string `env:"APP_ENV"`
	GinMode      string `env:"GIN_MODE"`
	DBDebug      bool   `env:"DB_DEBUG"`
	DBDebugLevel int    `env:"DB_DEBUG_LEVEL"`
}
