package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"gitlab.com/cahya-marine/water-level-project/entity"
	"gitlab.com/cahya-marine/water-level-project/pkg/gin_internal"
	"gitlab.com/cahya-marine/water-level-project/pkg/response"
	"gitlab.com/cahya-marine/water-level-project/service"
	"net/http"
)

type Actual struct {
	actualSrv service.ActualSrvItf
}

func NewActual(actualSrv service.ActualSrvItf) *Actual {
	return &Actual{actualSrv: actualSrv}
}

func (ctl *Actual) GetRawData(ctx *gin.Context) {

	payload, err := gin_internal.BindParams[entity.GetActualParams](ctx)
	if err != nil {
		response.HandleHTTPError(ctx, err)
		return
	}
	data, err := ctl.actualSrv.GetRawData(ctx, payload)
	if err != nil {
		response.HandleHTTPError(ctx, err)
		return
	}

	response.HandleHTTPSuccess(ctx, http.StatusOK, data)
	return

}

func (ctl *Actual) GetActualWeather(ctx *gin.Context) {

	payload, err := gin_internal.BindParams[entity.GetActualData](ctx)
	if err != nil {
		response.HandleHTTPError(ctx, err)
		return
	}
	data, err := ctl.actualSrv.GetActualData(ctx, payload)
	if err != nil {
		response.HandleHTTPError(ctx, err)
		return
	}

	response.HandleHTTPSuccess(ctx, http.StatusOK, data)
	return

}

func (ctl *Actual) CreateActual(ctx *gin.Context) {
	var payload entity.ActualDataRequest
	if err := ctx.ShouldBindBodyWith(&payload, binding.JSON); err != nil {
		response.HandleHTTPError(ctx, err)
		return
	}

	err := ctl.actualSrv.Create(ctx, payload)
	if err != nil {
		response.HandleHTTPError(ctx, err)
		return
	}

	response.HandleHTTPSuccess(ctx, http.StatusCreated, []string{})
	return
}
