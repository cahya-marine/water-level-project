package routers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cahya-marine/water-level-project/http_server/controllers"
	"gitlab.com/cahya-marine/water-level-project/http_server/middlewares"
	"gitlab.com/cahya-marine/water-level-project/repository"
	"gitlab.com/cahya-marine/water-level-project/service"
)

type RouterManager struct {
	handler *gin.Engine
}

func NewRouteManager(handler *gin.Engine) *RouterManager {
	return &RouterManager{
		handler: handler,
	}
}

func (r *RouterManager) Register() {
	r.handler.Use(middlewares.CORSMiddleware())

	actualRepo := repository.NewActualRepository()
	actualSrv := service.NewActual(*actualRepo)
	actualController := controllers.NewActual(actualSrv)

	r.handler.GET("/v1/weather/raw-data", actualController.GetRawData)
	r.handler.POST("/v1/weather/raw-data", actualController.CreateActual)
	r.handler.GET("/v1/weather/actual", actualController.GetActualWeather)
}
