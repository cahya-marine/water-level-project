package repository

import (
	"context"

	"github.com/uptrace/bun"
	db "gitlab.com/cahya-marine/water-level-project/database"
	"gitlab.com/cahya-marine/water-level-project/entity"
)

type Form1Repository struct {
	db *bun.DB
}

func NewForm1Repository() *Form1Repository {
	return &Form1Repository{
		db: db.GetConn(),
	}
}

func (repo *Form1Repository) Create(ctx context.Context, tx *bun.Tx, src []*entity.Form1) error {
	_, err := tx.NewInsert().
		Model(&src).
		Returning("id").
		Exec(ctx)
	if err != nil {
		return err
	}

	return nil
}
