package repository

import (
	"context"
	"strings"

	"github.com/uptrace/bun"
	db "gitlab.com/cahya-marine/water-level-project/database"
	"gitlab.com/cahya-marine/water-level-project/entity"
	"gitlab.com/cahya-marine/water-level-project/pkg/response"
)

type ActualRepository struct {
	db *bun.DB
}

func NewActualRepository() *ActualRepository {
	return &ActualRepository{
		db: db.GetConn(),
	}
}

func (repo *ActualRepository) Create(ctx context.Context, tx *bun.Tx, src []*entity.Actual) error {
	_, err := tx.NewInsert().
		Model(&src).
		Returning("id").
		Exec(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (repo *ActualRepository) CreateSingle(ctx context.Context, src entity.Actual) error {
	_, err := db.GetConn().NewInsert().
		Model(&src).
		Returning("id").
		On("CONFLICT (datetime) DO UPDATE").
		Exec(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (repo *ActualRepository) GetDetail(ctx context.Context, id int64) error {
	var result entity.Actual
	err := db.GetConn().NewSelect().
		Model((*entity.Actual)(nil)).
		Where("id = ?", id).
		Scan(ctx, &result)
	if err != nil {
		return err
	}

	return nil
}

func (r *ActualRepository) GetRawData(ctx context.Context, payload entity.GetActualParams) ([]entity.ActualResponse, int, error) {

	var (
		log []entity.ActualResponse
	)
	offset := response.CalculateOffset(payload.Limit, payload.Page)

	q := r.db.NewSelect().Model(&log)

	if len(payload.Date) != 0 {
		dateRangeStr := strings.Split(strings.ToLower(payload.Date), "/")
		q.Where("datetime BETWEEN ? AND ?", dateRangeStr[0], dateRangeStr[1])
	}
	total, err := q.Limit(payload.Limit).
		Offset(offset).
		Order("datetime ASC").
		ScanAndCount(ctx)

	if err != nil {
		return log, total, err
	}

	return log, total, nil
}

func (r *ActualRepository) GetActualData(ctx context.Context, payload entity.GetActualData) (res entity.ActualResponse, err error) {

	if err := r.db.NewSelect().
		Model(&res).
		Where("datetime BETWEEN ? AND ? ", payload.Date, payload.Date.AddDate(0, 0, 1)).
		Scan(ctx); err != nil {

		return res, err
	}
	return res, nil
}
