package repository

import (
	"context"

	"github.com/uptrace/bun"
	db "gitlab.com/cahya-marine/water-level-project/database"
	"gitlab.com/cahya-marine/water-level-project/entity"
)

type Form2Repository struct {
	db *bun.DB
}

func NewForm2Repository() *Form2Repository {
	return &Form2Repository{
		db: db.GetConn(),
	}
}

func (repo *Form2Repository) Create(ctx context.Context, tx *bun.Tx, src []*entity.Form2) error {
	_, err := tx.NewInsert().
		Model(&src).
		Returning("id").
		Exec(ctx)
	if err != nil {
		return err
	}

	return nil
}
