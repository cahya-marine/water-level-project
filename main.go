package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/cahya-marine/water-level-project/config"
	db "gitlab.com/cahya-marine/water-level-project/database"
	"gitlab.com/cahya-marine/water-level-project/pkg/http_server"
)

func main() {
	c := config.InitializeConfig()

	sc := make(chan os.Signal, 1)
	signal.Notify(
		sc,
		os.Interrupt,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)

	dbInstance := db.InitDB(&c.Database)
	defer dbInstance.Close()

	httpServer := http_server.InitAndStart(c.Application)

	<-sc

	err := httpServer.Shutdown(context.Background())
	if err != nil {
		log.Fatalf("failed to shutdown http server: %v", err)
	}

	fmt.Println("server shutdown gracefully")

}
