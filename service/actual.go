package service

import (
	"context"

	"gitlab.com/cahya-marine/water-level-project/entity"

	"gitlab.com/cahya-marine/water-level-project/pkg/response"
	"gitlab.com/cahya-marine/water-level-project/repository"
)

type ActualSrvItf interface {
	GetRawData(ctx context.Context, params entity.GetActualParams) (response.PagingResponse, error)
	GetActualData(ctx context.Context, params entity.GetActualData) ([]entity.Data, error)
	Create(ctx context.Context, payload entity.ActualDataRequest) error
}

type ActualSrv struct {
	actualRepo repository.ActualRepository
}

func NewActual(actualRepo repository.ActualRepository) ActualSrvItf {
	return &ActualSrv{
		actualRepo: actualRepo,
	}
}

func (s *ActualSrv) GetRawData(ctx context.Context, payload entity.GetActualParams) (response.PagingResponse, error) {

	data, count, err := s.actualRepo.GetRawData(ctx, payload)

	if err != nil {
		return response.PagingResponse{}, err
	}
	return response.NewPagingResponse(payload.Limit, payload.Page, count, len(data), data), nil
}

func (s *ActualSrv) GetActualData(ctx context.Context, payload entity.GetActualData) ([]entity.Data, error) {

	data, err := s.actualRepo.GetActualData(ctx, payload)

	if err != nil {
		return []entity.Data{}, err
	}
	return entity.ToActualResponse(data), nil
}

func (s *ActualSrv) Create(ctx context.Context, payload entity.ActualDataRequest) error {
	var (
		record                entity.Actual
		weatherLocationMap    = make(map[string]string)
		waterLevelLocationMap = make(map[string]float64)
	)

	year, month, day := payload.Date.Date()

	// Extract the time part
	timePart := payload.Date.Format("15:04:05")

	for _, item := range payload.RawData {
		waterLevelLocationMap[item.Location] = item.WaterLevel
		weatherLocationMap[item.Location] = item.Weather
	}

	record = entity.Actual{
		Datetime: entity.CustomDate{
			Time: payload.Date,
		},
		Date:                       day,
		Month:                      month.String(),
		Hours:                      timePart,
		Year:                       year,
		ActualWeatherPurukCahu:     weatherLocationMap["Puruk Cahu"],
		ActualWeatherJamut:         weatherLocationMap["Jamut"],
		ActualWeatherTahup:         weatherLocationMap["Tahup"],
		ActualWeatherMuaraTaweh:    weatherLocationMap["Muara Teweh"],
		ActualWeatherBintangNinggi: weatherLocationMap["Bintang Ninggi"],
		WaterLevelBintangNinggi:    waterLevelLocationMap["Bintang Ninggi"],
		WaterLevelTelukSiwak:       waterLevelLocationMap["Teluk Siwak"],
		WaterLevelPurukCahu:        waterLevelLocationMap["Puruk Cahu"],
		WaterLevelJamut:            waterLevelLocationMap["Jamut"],
		WaterLevelTuhup:            waterLevelLocationMap["Tahup"],
		WaterLevelMuaraTaweh:       waterLevelLocationMap["Muara Teweh"],
		WaterLevelTerusan:          waterLevelLocationMap["Terusan"],
	}

	err := s.actualRepo.CreateSingle(ctx, record)
	if err != nil {
		return err
	}

	return nil
}
